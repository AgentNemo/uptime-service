package checker

import (
	"encoding/json"
	"fmt"
	"gitlab.com/AgentNemo/uptime-service/logging"
	"gitlab.com/AgentNemo/uptime-service/models"
	"net/http"
	"time"
)

// HTTP Checker
type Http struct {
	Timeout  time.Duration
	Endpoint string
}

func NewHttp(endpoint string) *Http {
	return &Http{Timeout: time.Second * 5, Endpoint: endpoint}
}

func (h Http) Check(object *models.Object, logger logging.Logger) {
	endpoint := fmt.Sprintf(h.Endpoint, object.ObjectID)
	logger = logger.StartContext(nil, "HTTP Checker", "endpoint", endpoint)
	defer logger.FinishContext()
	client := http.Client{
		Timeout: h.Timeout,
	}

	req, err := http.NewRequest("GET", endpoint, nil)
	if err != nil {
		logger.InfoW(err.Error())
		SetFailure(object)
		return
	}
	req = req.WithContext(logger.Context())
	resp, err := client.Do(req)
	if err != nil {
		logger.InfoW(err.Error())
		SetFailure(object)
		return
	}
	if resp.StatusCode < 200 || resp.StatusCode >= 400 {
		logger.InfoW(fmt.Sprintf("StatusCode %d", resp.StatusCode))
		SetFailure(object)
		return
	}
	// TODO: outsource to model with automatic validation
	data := map[string]interface{}{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		logger.InfoW(err.Error())
		SetFailure(object)
		return
	}
	if _, ok := data["id"]; ok {
		if val, ok := data["online"]; ok {
			if online, ok := val.(bool); ok && online {
				SetSuccessful(object)
				return
			}
		}
	}
	SetFailure(object)
	return
}
