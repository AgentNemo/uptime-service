package tracing

import (
	"fmt"
	"github.com/opentracing/opentracing-go"
	"github.com/uber/jaeger-client-go"
	"github.com/uber/jaeger-client-go/config"
	"github.com/uber/jaeger-lib/metrics/prometheus"
	"gitlab.com/AgentNemo/uptime-service/logging"
	"io"
	"os"
	"strconv"
	"time"
)

type Jaeger struct {
	Logger jaeger.Logger
	Closer io.Closer
}

type JaegerLogger struct {
	logging.Logger
}

func (j JaegerLogger) Error(msg string) {
	j.Logger.ErrorW(msg)
}

func (j JaegerLogger) Infof(msg string, args ...interface{}) {
	j.Logger.DebugW(fmt.Sprintf(msg, args...))
}

//NewJaeger creates a new jaeger instance and inject the instance into the global trace
func NewJaeger(logger logging.Logger) *Jaeger {
	j := &Jaeger{}
	value, err := strconv.ParseBool(os.Getenv("OPENTRACING_ACTIVATED"))
	if err != nil {
		return j
	}
	if !value {
		return j
	}
	metricsFactory := prometheus.New()
	configT := &config.Configuration{}
	configT.Reporter = &config.ReporterConfig{
		LogSpans:                 true,
		AttemptReconnectInterval: time.Second,
	}
	configT.ServiceName = os.Getenv("SERVICE_NAME")
	configT.Tags = append(configT.Tags, opentracing.Tag{
		Key:   "SERVICE_VERSION",
		Value: os.Getenv("SERVICE_VERSION"),
	})
	configT.Tags = append(configT.Tags, opentracing.Tag{
		Key:   "SERVICE_STAGE",
		Value: os.Getenv("SERVICE_STAGE"),
	})
	configT.Tags = append(configT.Tags, opentracing.Tag{
		Key:   "LOGGER_STAGE",
		Value: os.Getenv("LOGGER_STAGE"),
	})
	configT.Sampler = &config.SamplerConfig{
		Type:  jaeger.SamplerTypeConst,
		Param: 1,
	}
	configT, err = configT.FromEnv()
	if err != nil {
		return j
	}
	var loggerJaeger jaeger.Logger = jaeger.StdLogger
	if logger != nil {
		loggerJaeger = JaegerLogger{logger}
	}
	tracer, closer, err := configT.NewTracer(
		config.Metrics(metricsFactory),
		config.Logger(loggerJaeger),
	)
	if err != nil {
		return j
	}
	j.Logger = loggerJaeger
	j.Closer = closer
	opentracing.SetGlobalTracer(tracer)
	return j
}
func (j *Jaeger) Close() {
	if j.Closer == nil {
		return
	}
	j.Closer.Close()
}
