package controller

import (
	"fmt"
	"gitlab.com/AgentNemo/uptime-service/db"
	"gitlab.com/AgentNemo/uptime-service/logging"
	"gitlab.com/AgentNemo/uptime-service/models"
	"time"
)

type Deleter struct {
	DB             *db.DB
	Logger         logging.Logger
	DeleteDuration time.Duration
	CheckInterval  time.Duration
}

// NewChecker - creates controller for handling checking for objects
func NewDeleter(DB *db.DB, logger logging.Logger) *Deleter {
	return &Deleter{DB: DB, Logger: logger, DeleteDuration: time.Second * 30, CheckInterval: time.Second}
}

func (d *Deleter) SetDeleteDuration(duration time.Duration) *Deleter {
	d.DeleteDuration = duration
	return d
}

func (d *Deleter) SetCheckInterval(duration time.Duration) *Deleter {
	d.CheckInterval = duration
	return d
}

// Starts Hot loop
func (d *Deleter) Start(kill chan bool) {
	d.Logger.InfoW("Start Deleter loop")
	timer := time.AfterFunc(d.CheckInterval, d.fetch)
	for true {
		select {
		case msg1 := <-kill:
			d.Logger.InfoW(fmt.Sprintf("kill %v", msg1))
			timer.Stop()
			return
		case msg2 := <-timer.C:
			d.Logger.DebugW(fmt.Sprintf("timer %v", msg2))
			timer.Reset(d.CheckInterval)
		}
	}
}

func (d *Deleter) fetch() {
	logger := d.Logger.StartContext(d.Logger.Context(), "Deleting")
	objects, err := models.NewObject(d.DB).GetAll(logger.Context())
	if err != nil {
		logger.ErrorW(err.Error())
	}
	for _, object := range objects {
		if time.Now().Sub(object.LastSeen) >= d.DeleteDuration {
			err := object.Delete(logger.Context())
			if err != nil {
				logger.ErrorW(err.Error())
			}
			logger.InfoW(fmt.Sprintf("object %d was deleted", object.ObjectID))
		}
	}
	logger.FinishContext()
}
