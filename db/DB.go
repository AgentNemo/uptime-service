package db

import (
	"gitlab.com/AgentNemo/uptime-service/logging"
	"gorm.io/gorm"
	"time"
)

// database wrapper arround gorm
type DB struct {
	*gorm.DB
}

// creates a new database object
func NewDB(logger logging.Logger) (*DB, error) {
	db, err := gorm.Open(NewPostgresByEnv(), &gorm.Config{
		Logger: logger,
	})
	if err != nil {
		return nil, err
	}
	sql, err := db.DB()
	if err == nil {
		sql.SetMaxIdleConns(10)
		sql.SetMaxOpenConns(100)
		sql.SetConnMaxLifetime(time.Hour)
	}
	return &DB{db}, nil
}

func NewDBWithDialector(dialector gorm.Dialector, config gorm.Config) (*DB, error) {
	db, err := gorm.Open(dialector, &config)
	if err != nil {
		return nil, err
	}
	return &DB{db}, nil
}

func (d *DB) ApplySchemas(schemas ...interface{}) {
	d.DB.AutoMigrate(schemas...)
}
