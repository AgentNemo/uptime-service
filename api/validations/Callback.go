package validations

import (
	"context"
	"encoding/json"
	"github.com/go-playground/validator/v10"
	"gitlab.com/AgentNemo/uptime-service/api/validations/models"
	"gitlab.com/AgentNemo/uptime-service/logging"
	"io"
)

// validation func for the callback endpoint
func ValidatorCallback() func(payload io.ReadCloser, validator *validator.Validate, logger logging.Logger) (interface{}, error) {
	return func(payload io.ReadCloser, validator *validator.Validate, logger logging.Logger) (interface{}, error) {
		logger.StartContext(context.Background(), "validation callback")
		defer logger.FinishContext()
		data := models.POSTCallBack{}
		err := json.NewDecoder(payload).Decode(&data)
		if err != nil {
			logger.ErrW(err)
			return nil, ErrorJson
		}
		err = validator.Struct(data)
		if err != nil {
			logger.ErrW(err)
			return nil, ErrorValidation
		}
		if len(data.ObjectIds) == 0 {
			return nil, ErrorValidation
		}
		return data, nil
	}
}
