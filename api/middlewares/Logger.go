package middlewares

import (
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/AgentNemo/uptime-service/logging"
	"net/http"
)

// Custom Logger Middleware
func Logger(logger logging.Logger) func(next http.Handler) http.Handler {
	return middleware.RequestLogger(&middleware.DefaultLogFormatter{Logger: logger, NoColor: true})
}
