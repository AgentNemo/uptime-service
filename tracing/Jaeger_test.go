package tracing

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/AgentNemo/uptime-service/logging"
	"os"
	"testing"
)

func TestNewJaeger_Failed(t *testing.T) {
	jaegerTracer := NewJaeger(nil)
	assert.NotNil(t, jaegerTracer)
	assert.Nil(t, jaegerTracer.Logger)
	assert.Nil(t, jaegerTracer.Closer)
}

func TestNewJaeger_Suc(t *testing.T) {
	os.Setenv("OPENTRACING_ACTIVATED", "True")
	os.Setenv("SERVICE_NAME", "Test")
	jaegerTracer := NewJaeger(logging.NewZapDevelopment())
	assert.NotNil(t, jaegerTracer)
	assert.NotNil(t, jaegerTracer.Logger)
	assert.NotNil(t, jaegerTracer.Closer)
	assert.IsType(t, jaegerTracer.Logger, JaegerLogger{})
}
