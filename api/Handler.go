package api

import (
	"fmt"
	"gitlab.com/AgentNemo/uptime-service/api/validations"
	"gitlab.com/AgentNemo/uptime-service/api/validations/models"
	"gitlab.com/AgentNemo/uptime-service/checker"
	"gitlab.com/AgentNemo/uptime-service/db"
	"gitlab.com/AgentNemo/uptime-service/logging"
	models2 "gitlab.com/AgentNemo/uptime-service/models"
	"net/http"
	"sync"
)

type Handler struct {
	DB     *db.DB
	Logger logging.Logger
}

func NewHandler(DB *db.DB, logger logging.Logger) *Handler {
	return &Handler{DB: DB, Logger: logger}
}

// Callback godoc
// @Summary Triggers online check
// @Description Triggers an online check for every id given
// @Param payload body models.POSTCallBack true "ids"
// @Success 200
// @Failure 400
// @Failure 415
// @Failure 422
// @Router /callback [post]
func (h *Handler) Callback(ichecker checker.IChecker) validations.CustomHandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request, payload interface{}) {
		data := payload.(models.POSTCallBack)
		logger := h.Logger.StartContext(request.Context(), "Callback", "IDs", fmt.Sprintf("%v", data.ObjectIds))
		defer logger.FinishContext()
		var objects []*models2.Object
		wg := sync.WaitGroup{}
		for _, id := range data.ObjectIds {
			object := models2.NewObject(h.DB).SetID(uint(id))
			objects = append(objects, object)
			wg.Add(1)
			go checkObject(object, ichecker, &wg, logger)
		}
		wg.Wait()
		writer.WriteHeader(http.StatusOK)
	}
}

// checks an object
func checkObject(object *models2.Object, iChecker checker.IChecker, wg *sync.WaitGroup, logger logging.Logger) {
	logger = logger.StartContext(nil, "check object", "ID", object.ObjectID)
	defer wg.Done()
	defer logger.FinishContext()
	err := object.Read(logger.Context())
	if err == nil {
		logger.DebugW("object found", "ID", object.ObjectID)
		iChecker.Check(object, logger)
		err := object.Save(logger.Context())
		if err != nil {
			logger.InfoW(err.Error())
		}
		return
	}
	iChecker.Check(object, logger)
	err = object.Create(logger.Context())
	if err != nil {
		logger.InfoW(err.Error())
	}
}
