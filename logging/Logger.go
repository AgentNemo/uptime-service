package logging

import (
	"context"
	"fmt"
	"github.com/opentracing/opentracing-go"
	otlog "github.com/opentracing/opentracing-go/log"
	"github.com/uber/jaeger-client-go"
	"go.uber.org/zap"
	glog "gorm.io/gorm/logger"
	"os"
	"time"
)

type zapLogger struct {
	*zap.SugaredLogger
	span opentracing.Span
	ctx  context.Context
}

// Creates a new logger. The type is depending from the env variable "LOGGER_STAGE"
func NewZapByEnv() Logger {
	value, exists := os.LookupEnv("LOGGER_STAGE")
	if !exists {
		return NewZapDevelopment()
	}
	if value == "prod" || value == "Prod" || value == "PROD" {
		return NewZapProduction()
	}
	return NewZapDevelopment()
}

// Creates a new deployment logger
func NewZapDevelopment() Logger {
	config := zap.NewDevelopmentConfig()
	customizeConfig(&config)
	return NewLoggerByConfig(config)
}

// Creates a new production logger
func NewZapProduction() Logger {
	config := zap.NewProductionConfig()
	customizeConfig(&config)
	return NewLoggerByConfig(config)
}

// Creates a new logger with the specified configuration
func NewLoggerByConfig(config zap.Config) Logger {
	logger, err := config.Build(zap.AddCaller(), zap.AddCallerSkip(1))
	if err != nil {
		return NopLogger()
	}
	return newZap(logger)
}

// Creates a new NOP Logger
func NopLogger() Logger {
	return newZap(zap.NewNop())
}

// Create lib specific logger
func newZap(log *zap.Logger) Logger {
	return &zapLogger{
		SugaredLogger: log.Sugar(),
	}
}

// inject configuration into the current configuration depending on env variables.
// LOGGER_FILE_PATH - if set logs are also written to files
func customizeConfig(config *zap.Config) {
	path, exists := os.LookupEnv("LOGGER_FILE_PATH")
	if !exists {
		return
	}
	config.OutputPaths = append(config.OutputPaths, path)
}

// Creates a new child logger with a new context
func (l *zapLogger) StartContext(ctx context.Context, name string, kv ...interface{}) Logger {
	if ctx == nil {
		if l.ctx == nil {
			return l
		}
		ctx = l.ctx
	}
	operationName := ""
	if len(name) == 0 {
		funcName, fileName, fileLine, err := Here(1)
		if err == nil {
			operationName = fmt.Sprintf("%s - %s - %d", funcName, fileName, fileLine)
		}
	} else {
		operationName = name
	}
	span, ctxN := opentracing.StartSpanFromContext(ctx, operationName)
	if span != nil {
		span.LogKV(kv...)
	}
	newLogger := &zapLogger{
		SugaredLogger: l.SugaredLogger,
		span:          span,
		ctx:           ctxN,
	}
	return newLogger
}

// Finishes the context
func (l *zapLogger) FinishContext() {
	if l.span != nil {
		l.span.Finish()
	}
	if l.ctx != nil {
		l.ctx.Done()
	}
}

func (l *zapLogger) Context() context.Context {
	if l.ctx != nil {
		return l.ctx
	}
	return context.Background()
}

// Injects tracer information into the list if a span is alive
func (l *zapLogger) injectTracerInformation(kv []interface{}) []interface{} {
	if l.span == nil {
		return kv
	}
	traceID := ""
	spanID := ""
	if jaegerCtx, ok := l.span.Context().(jaeger.SpanContext); ok {
		traceID = jaegerCtx.TraceID().String()
		spanID = jaegerCtx.SpanID().String()
	}
	if traceID != "" {
		kv = append(kv, "trace_id", traceID)
	}
	if spanID != "" {
		kv = append(kv, "span_id", spanID)
	}
	return kv
}

// Logs a span
func (l *zapLogger) logSpan(level string, msg string, kv ...interface{}) {
	if l.ctx == nil {
		return
	}
	span, _ := opentracing.StartSpanFromContext(l.ctx, msg)
	defer span.Finish()
	span.LogFields(
		otlog.String("level", level),
	)
	span.LogKV(kv...)
}

func (l *zapLogger) DebugW(msg string, kv ...interface{}) {
	l.logSpan("debug", msg, kv...)
	l.SugaredLogger.Debugw(msg, l.injectTracerInformation(kv)...)
}

func (l *zapLogger) InfoW(msg string, kv ...interface{}) {
	l.logSpan("info", msg, kv...)
	l.SugaredLogger.Infow(msg, l.injectTracerInformation(kv)...)
}

func (l *zapLogger) WarnW(msg string, kv ...interface{}) {
	l.logSpan("warn", msg, kv...)
	l.SugaredLogger.Warnw(msg, l.injectTracerInformation(kv)...)
}

func (l *zapLogger) ErrorW(msg string, kv ...interface{}) {
	l.logSpan("error", msg, kv...)
	l.SugaredLogger.Errorw(msg, l.injectTracerInformation(kv)...)
}

func (l *zapLogger) FatalW(msg string, kv ...interface{}) {
	l.logSpan("fatal", msg, kv...)
	l.SugaredLogger.Fatalw(msg, l.injectTracerInformation(kv)...)
}

func (l *zapLogger) ErrW(err error, kv ...interface{}) {
	l.ErrorW(err.Error(), kv...)
}

// DB integration

func (l *zapLogger) LogMode(level glog.LogLevel) glog.Interface {
	switch level {
	case glog.Silent:
		return NopLogger()
	default:
		return l
	}
}

func (l *zapLogger) Info(ctx context.Context, s string, i ...interface{}) {
	l.StartContext(ctx, "DB info")
	l.InfoW(fmt.Sprintf(s, i...))
	l.FinishContext()
}

func (l *zapLogger) Warn(ctx context.Context, s string, i ...interface{}) {
	l.StartContext(ctx, "DB warn")
	l.WarnW(fmt.Sprintf(s, i...))
	l.FinishContext()
}

func (l *zapLogger) Error(ctx context.Context, s string, i ...interface{}) {
	l.StartContext(ctx, "DB error")
	l.ErrorW(fmt.Sprintf(s, i...))
	l.FinishContext()
}

func (l *zapLogger) Trace(ctx context.Context, begin time.Time, fc func() (sql string, rowsAffected int64), err error) {
	logger := l.StartContext(ctx, "DB trace")
	defer logger.FinishContext()
	sql, rows := fc()
	end := time.Now()
	if err != nil {
		logger.DebugW("trace",
			"begin", begin.String(), "end", end.String(), "duration", end.Sub(begin).String(),
			"sql", sql, "rows", rows, "error", err.Error())
	} else {
		logger.DebugW("trace",
			"begin", begin.String(), "end", end.String(), "duration", end.Sub(begin).String(),
			"sql", sql, "rows", rows)
	}
}

// middleware integration

func (l *zapLogger) Print(args ...interface{}) {
	l.InfoW(fmt.Sprint(args...))
}
