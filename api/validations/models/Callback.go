package models

// API schema for the calllback endpoint
type POSTCallBack struct {
	// object ids to check
	ObjectIds []int `json:"object_ids" validator:"required,min=1"`
}
