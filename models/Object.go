package models

import (
	"context"
	"gitlab.com/AgentNemo/uptime-service/db"
	"time"
)

type Object struct {
	ID            uint `json:"-"`
	ObjectID      uint `json:"id"`
	LastRequested time.Time
	LastSeen      time.Time
	FirstSeen     time.Time
	Online        bool
	DB            *db.DB `json:"-" gorm:"-" sql:"-"`
}

func NewObject(DB *db.DB) *Object {
	return &Object{DB: DB}
}

func (o *Object) SetID(id uint) *Object {
	o.ObjectID = id
	return o
}

func (o *Object) Create(ctx context.Context) error {
	return o.DB.WithContext(ctx).Create(o).Error
}

func (o *Object) Save(ctx context.Context) error {
	return o.DB.WithContext(ctx).Save(o).Error
}

func (o *Object) Read(ctx context.Context) error {
	return o.DB.WithContext(ctx).Where(o).First(o).Error
}

func (o *Object) GetAll(ctx context.Context) ([]*Object, error) {
	objects := make([]*Object, 0)
	err := o.DB.WithContext(ctx).Where(o).Find(&objects).Error
	if err != nil {
		return nil, err
	}
	for _, object := range objects {
		object.DB = o.DB
	}
	return objects, nil
}

func (o *Object) Delete(ctx context.Context) error {
	return o.DB.WithContext(ctx).Where(o).Delete(o).Error
}
