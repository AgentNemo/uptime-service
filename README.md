# Uptime service

A simple service for checking the uptime of other services.

The service listens on the endpoint ``/callback`` at the HTTP method PUT, which takes
 as payload a list of ids. The ids are used to call the services endpoint to check the online state.
 
 The response of the service, whcih are checked, needs to match the schema:
 ````json
{
"id": <id>,
"online": true|false
}
````

The information are saved into a postgres DB. Services which have not been updated for 30 seconds
are automatically deleted from the DB.

A script for calling the service ``/callback`` endpoint and mocking the services online state can be found be ``client``

# Requirements

- Docker
- docker-compose
- go 1.16

# Start Up

Using ``docker-compose`` is the best way to start the service with the dependencies and extras

````shell script
docker-compose up
````

# Swagger

The swagger doc can be accessed at ``http://localhost:9090/swagger/index.html``

# Monitoring

A Jaeger instance is deployed and can be accessed at ``http://localhost:16686/``

# Tests

````shell script
go test -covermode=atomic -coverprofile=c.out ./... && go tool cover -html=c.out -o coverage.html
````

# ENV

Provide an ``.env`` file in the root dir.

Example:

````
POSTGRES_USER=uptimer
POSTGRES_PASSWORD=uptimer_pass
POSTGRES_DB=uptime
POSTGRES_PORT=5432
POSTGRES_HOST=database
LOGGER_STAGE=PROD
ROUTER_PORT=9090
SERVICE_NAME=Uptime
SERVICE_VERSION=0.1.0
SERVICE_STAGE=PROD
CHECKER_HTTP_URL=client:9010/objects/%s
OPENTRACING_ACTIVATED=True
JAEGER_SAMPLING_ENDPOINT=http://jaeger:5778/sampling
JAEGER_AGENT_HOST=jaeger
SWAGGER_HOST="localhost:9090"
````