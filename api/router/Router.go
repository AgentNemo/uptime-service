package router

import (
	"errors"
	"fmt"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/httptracer"
	"github.com/go-playground/validator/v10"
	"github.com/opentracing/opentracing-go"
	"gitlab.com/AgentNemo/uptime-service/api/middlewares"
	"gitlab.com/AgentNemo/uptime-service/api/validations"
	"gitlab.com/AgentNemo/uptime-service/logging"
	"net/http"
	"os"
)

// Router - Wrapped around chi router with steriods
type Router struct {
	*chi.Mux
	Logger    logging.Logger
	Config    Config
	Validator *validator.Validate
}

// Creates a new Router from a logger and a config
func NewRouter(logger logging.Logger, config Config) *Router {
	return &Router{chi.NewRouter(), logger, config, validator.New()}
}

// Creates a new Router with default configuration
func NewRouterDefault(logger logging.Logger) *Router {
	router := NewRouter(logger, getConfigByEnv())
	router.setMiddlewareDefault()
	return router
}

// Returns a configuration from env variables
func getConfigByEnv() Config {
	port := os.Getenv("ROUTER_PORT")
	if len(port) == 0 {
		port = "9090"
	}
	return Config{Port: port}
}

// Sets the default middleware
func (r *Router) setMiddlewareDefault() {
	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middlewares.Logger(r.Logger))
	r.Use(middleware.Recoverer)
	r.Use(httptracer.Tracer(opentracing.GlobalTracer(), httptracer.Config{
		ServiceName:    os.Getenv("SERVICE_NAME"),
		ServiceVersion: os.Getenv("SERVICE_VERSION"),
		OperationName:  os.Getenv("SERVICE_NAME"),
		SkipFunc: func(r *http.Request) bool {
			return r.URL.Path == "/health"
		},
		Tags: map[string]interface{}{
			"ROUTER_PORT":   os.Getenv("ROUTER_PORT"),
			"SERVICE_STAGE": os.Getenv("SERVICE_STAGE")},
	}))
}

// Starts the API - blocking
func (r *Router) Start() error {
	r.Logger.InfoW(fmt.Sprintf("Listen and Serve on port %s", r.Config.Port), "Port", r.Config.Port)
	return http.ListenAndServe(fmt.Sprintf(":%s", r.Config.Port), r)
}

// Registers a PUT endpoint with validation
func (r Router) PutWithValidation(pattern string, handlerFn validations.CustomHandlerFunc, validator validations.Validator) {
	r.Put(pattern, r.wrapValidate(validator, handlerFn))
}

// Registers a POST endpoint with validation
func (r Router) PostWithValidation(pattern string, handlerFn validations.CustomHandlerFunc, validator validations.Validator) {
	r.Post(pattern, r.wrapValidate(validator, handlerFn))
}

// Wraps a validation call around a http handler
func (r Router) wrapValidate(validator validations.Validator, handlerFn validations.CustomHandlerFunc) func(http.ResponseWriter, *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {
		data, err := validator(request.Body, r.Validator, r.Logger)
		if err != nil {
			if errors.Is(err, validations.ErrorJson) {
				writer.WriteHeader(http.StatusUnsupportedMediaType)
				return
			}
			if errors.Is(err, validations.ErrorValidation) {
				writer.WriteHeader(http.StatusUnprocessableEntity)
				return
			}
			writer.WriteHeader(http.StatusBadRequest)
		}
		handlerFn(writer, request, data)
	}
}
