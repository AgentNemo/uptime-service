package validations

import (
	"github.com/cockroachdb/errors"
	"github.com/go-playground/validator/v10"
	"gitlab.com/AgentNemo/uptime-service/logging"
	"io"
	"net/http"
)

// validation errors
var (
	ErrorJson       = errors.New("json error")
	ErrorValidation = errors.New("validation error")
)

// validations func
type Validator func(payload io.ReadCloser, validator *validator.Validate, logger logging.Logger) (interface{}, error)

// extends the http handler with the validated payload
type CustomHandlerFunc func(http.ResponseWriter, *http.Request, interface{})
