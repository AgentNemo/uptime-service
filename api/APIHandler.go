package api

import (
	httpSwagger "github.com/swaggo/http-swagger"
	"gitlab.com/AgentNemo/uptime-service/api/router"
	"gitlab.com/AgentNemo/uptime-service/api/validations"
	"gitlab.com/AgentNemo/uptime-service/checker"
	"gitlab.com/AgentNemo/uptime-service/db"
	"gitlab.com/AgentNemo/uptime-service/docs"
	"gitlab.com/AgentNemo/uptime-service/logging"
	"os"
)

// @title Uptime service
// @version 1.0
// @description Uptime service
// @termsOfService http://swagger.io/terms/

// @contact.name API Support
// @contact.url http://www.swagger.io/support
// @contact.email support@swagger.io

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html
func StartAPI(logger logging.Logger, database *db.DB) error {
	// Create router
	r := router.NewRouterDefault(logger)
	// Create handler
	handler := NewHandler(database, logger)
	// register func
	docs.SwaggerInfo.Host = os.Getenv("SWAGGER_HOST")
	r.Mount("/swagger", httpSwagger.WrapHandler)
	r.PostWithValidation("/callback",
		handler.Callback(checker.NewHttp(os.Getenv("CHECKER_HTTP_URL"))),
		validations.ValidatorCallback())
	return r.Start()
}
