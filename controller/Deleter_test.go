package controller

import (
	"context"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/AgentNemo/uptime-service/db"
	"gitlab.com/AgentNemo/uptime-service/logging"
	"gitlab.com/AgentNemo/uptime-service/models"
	"gorm.io/gorm"
	"regexp"
	"testing"
	"time"
)

func TestDeleter(t *testing.T) {
	dbRaw, mock, err := sqlmock.New()
	assert.Nil(t, err)

	defer dbRaw.Close()
	database, err := db.NewDBWithDialector(db.NewPostgresByDB(dbRaw), gorm.Config{})
	assert.Nil(t, err)

	mock.ExpectBegin()
	mock.ExpectQuery(regexp.QuoteMeta(`INSERT INTO "objects" ("object_id","last_requested","last_seen","first_seen","online") VALUES ($1,$2,$3,$4,$5) RETURNING "id"`)).
		WithArgs(15, time.Time{}, time.Time{}, time.Time{}, false).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow("1"))
	mock.ExpectCommit()

	mock.ExpectBegin()
	mock.ExpectQuery(regexp.QuoteMeta(`INSERT INTO "objects" ("object_id","last_requested","last_seen","first_seen","online") VALUES ($1,$2,$3,$4,$5) RETURNING "id"`)).
		WithArgs(20, time.Time{}, time.Time{}, time.Time{}, false).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow("1"))
	mock.ExpectCommit()

	object := models.NewObject(database)
	object.ObjectID = 15
	err = object.Create(context.Background())
	assert.Nil(t, err)

	objectTwo := models.NewObject(database)
	objectTwo.ObjectID = 20
	err = objectTwo.Create(context.Background())
	assert.Nil(t, err)

	mock.ExpectQuery(`SELECT \* FROM \"objects\"`)

	deleter := NewDeleter(database, logging.NopLogger())
	kill := make(chan bool)
	go deleter.Start(kill)
	time.Sleep(time.Second * 1)
	kill <- true

	assert.Nil(t, mock.ExpectationsWereMet())
}
