package db

import (
	"database/sql"
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"os"
)

// postgres connector
type Postgres struct {
	gorm.Dialector
}

func NewPostgres(connectionString string) Postgres {
	return Postgres{postgres.Open(connectionString)}
}

func NewPostgresByEnv() Postgres {
	return Postgres{postgres.Open(getDBConnStr())}
}

func NewPostgresByDB(db *sql.DB) Postgres {
	return Postgres{postgres.New(postgres.Config{Conn: db})}
}

func getDBConnStr() string {
	return fmt.Sprintf(
		"postgres://%s:%s@%s:%s/%s?sslmode=disable",
		os.Getenv("POSTGRES_USER"),
		os.Getenv("POSTGRES_PASSWORD"),
		os.Getenv("POSTGRES_HOST"),
		os.Getenv("POSTGRES_PORT"),
		os.Getenv("POSTGRES_DB"),
	)
}
