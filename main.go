package main

import (
	"gitlab.com/AgentNemo/uptime-service/api"
	"gitlab.com/AgentNemo/uptime-service/controller"
	"gitlab.com/AgentNemo/uptime-service/db"
	"gitlab.com/AgentNemo/uptime-service/logging"
	"gitlab.com/AgentNemo/uptime-service/models"
	"gitlab.com/AgentNemo/uptime-service/tracing"
	"log"
)

func main() {
	// initialize logger
	logger := logging.NewZapByEnv()
	// initialize tracer
	opentracer := tracing.NewJaeger(logger)
	defer opentracer.Close()
	// initialize db
	database, err := db.NewDB(logger)
	if err != nil {
		log.Fatal(err)
	}
	// apply models schema if needed
	database.ApplySchemas(&models.Object{})

	// start the automatically deleter
	deleter := controller.NewDeleter(database, logger)
	kill := make(chan bool)
	go deleter.Start(kill)
	// start API
	err = api.StartAPI(logger, database)
	if err != nil {
		logger.InfoW(err.Error())
	}
	kill <- true
}
