package checker

import (
	"gitlab.com/AgentNemo/uptime-service/logging"
	"gitlab.com/AgentNemo/uptime-service/models"
	"time"
)

// Interface for Object Checker
type IChecker interface {
	Check(object *models.Object, logger logging.Logger)
}

// Sets the object state to successful check
func SetSuccessful(object *models.Object) {
	object.Online = true
	if object.FirstSeen.IsZero() {
		object.FirstSeen = time.Now()
	}
	object.LastSeen = time.Now()
}

// Sets the object state to failure check
func SetFailure(object *models.Object) {
	object.Online = false
}
