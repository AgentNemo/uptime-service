module gitlab.com/AgentNemo/uptime-service

go 1.16

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/HdrHistogram/hdrhistogram-go v1.1.2 // indirect
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/cockroachdb/errors v1.8.6
	github.com/go-chi/chi/v5 v5.0.7
	github.com/go-chi/httptracer v0.3.0
	github.com/go-playground/validator/v10 v10.9.0
	github.com/opentracing/opentracing-go v1.2.0
	github.com/prometheus/client_golang v1.11.0 // indirect
	github.com/stretchr/testify v1.7.0
	github.com/swaggo/http-swagger v1.1.2
	github.com/swaggo/swag v1.7.8
	github.com/uber/jaeger-client-go v2.30.0+incompatible
	github.com/uber/jaeger-lib v2.4.1+incompatible
	go.uber.org/zap v1.19.1
	gorm.io/driver/postgres v1.2.1
	gorm.io/gorm v1.22.4
)
