package logging

import (
	"context"
	"fmt"
	glog "gorm.io/gorm/logger"
	"path/filepath"
	"runtime"
	"strings"
	"time"
)

// Logger Interface
type Logger interface {
	DebugW(msg string, kv ...interface{})
	// Logs with info level.
	InfoW(msg string, kv ...interface{})
	// Logs with warning level.
	WarnW(msg string, kv ...interface{})
	// Logs with error level.
	ErrorW(msg string, kv ...interface{})
	// Logs with fatal level.
	FatalW(msg string, kv ...interface{})
	// Logs with error level
	ErrW(err error, kv ...interface{})

	// returns a new logger from the context. if no context provided the context of the current logger is used
	// starts a new tracer span
	StartContext(ctx context.Context, name string, kv ...interface{}) Logger

	// finished the context and finishes the trace span
	FinishContext()

	// returns the current logger context. If no found a background context is returned
	Context() context.Context

	// database related logs
	LogMode(level glog.LogLevel) glog.Interface

	Info(ctx context.Context, s string, i ...interface{})

	Warn(ctx context.Context, s string, i ...interface{})

	Error(ctx context.Context, s string, i ...interface{})

	// logs a trace
	Trace(ctx context.Context, begin time.Time, fc func() (sql string, rowsAffected int64), err error)

	// router related logs
	Print(args ...interface{})
}

// Returns the current position of the caller
func Here(skip ...int) (string, string, int, error) {
	sk := 1
	if len(skip) > 0 && skip[0] > 1 {
		sk = skip[0]
	}
	var pc uintptr
	var ok bool
	pc, fileName, fileLine, ok := runtime.Caller(sk)
	if !ok {
		return "", "", 0, fmt.Errorf("N/A")
	}
	fn := runtime.FuncForPC(pc)
	name := fn.Name()
	ix := strings.LastIndex(name, ".")
	if ix > 0 && (ix+1) < len(name) {
		name = name[ix+1:]
	}
	funcName := name
	nd, nf := filepath.Split(fileName)
	fileName = filepath.Join(filepath.Base(nd), nf)
	return funcName, fileName, fileLine, nil
}
